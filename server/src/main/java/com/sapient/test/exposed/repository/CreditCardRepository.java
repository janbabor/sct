package com.sapient.test.exposed.repository;

import com.sapient.test.domain.entity.CreditCard;
import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
}
