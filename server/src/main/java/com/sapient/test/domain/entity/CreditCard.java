package com.sapient.test.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditCard implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private String cardNumber;

    @Column(columnDefinition = "Decimal(10,2) default '0.00'")
    private double balance;

    private int balanceLimit;

    public CreditCard(String name, String cardNumber, double balance, int balanceLimit) {
        this.name = name;
        this.cardNumber = cardNumber;
        this.balance = balance;
        this.balanceLimit = balanceLimit;
    }

}
