package com.sapient.test;

import com.sapient.test.domain.entity.CreditCard;
import com.sapient.test.exposed.repository.CreditCardRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }

    @Bean
    ApplicationRunner init(CreditCardRepository repository) {
        return args -> {
            CreditCard c1 = new CreditCard("Alice", "1111222233334444", -1045, 2000);
            CreditCard c2 = new CreditCard("Bob", "4444333322221111", 10.24, 5000);
            repository.save(c1);
            repository.save(c2);
        };
    }
}
