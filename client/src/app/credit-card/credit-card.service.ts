import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {CreditCard} from './credit-card';
import {HttpClient} from '@angular/common/http';
import {map, tap} from 'rxjs/operators';

/**
 * An injectable service which will be responsible for talking to the REST service's creditCard endpoint
 */
@Injectable({
  providedIn: 'root'
})
export class CreditCardService {


  private endpoint = 'http://localhost:9000/creditCards';

  /**
   * A list with the raw list data from the responses.
   */
  private creditCardsList$ = new BehaviorSubject<CreditCard[]>([]);

  /**
   * A observable stream of all credit cards in the system
   *
   * @return Observable<CreditCard[]> Returns a stream of a list of all credit cards in the system.
   */
  get creditCards$(): Observable<CreditCard[]> {
    return this.creditCardsList$.asObservable();
  }

  constructor(private httpClient: HttpClient) {
    this.fetchAll();
  }

  /**
   * A trigger function to reload the credit card list from the server.
   */
  fetchAll(): void {
    this.httpClient.get(this.endpoint)
      .pipe(
        map((response: any) => response['_embedded'].creditCards)
      )
      .subscribe((creditCards: CreditCard[]) => this.creditCardsList$.next(creditCards));
  }

  /**
   * Save a credit card to the system. Will trigger a credit card list reload upon a successful response
   *
   * @param creditCard A representation on the credit card that should be saved to the system.
   */
  save(creditCard: CreditCard): Observable<CreditCard> {
    return this.httpClient.post(this.endpoint, creditCard)
      .pipe(
        tap(() => this.fetchAll()),
        map((response: any) => response['_embedded'] as CreditCard)
      );
  }
}
