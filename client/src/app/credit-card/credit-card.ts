export interface CreditCard {
  id: number;
  name: string;
  cardNumber: string;
  balance: number;
  balanceLimit: number;
}
