import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreditCardCreateComponent} from './credit-card-create/credit-card-create.component';
import {CreditCardListComponent} from './credit-card-list/credit-card-list.component';
import {CreditCardComponent} from './credit-card.component';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [CreditCardCreateComponent, CreditCardListComponent, CreditCardComponent],
  exports: [CreditCardComponent]
})
export class CreditCardModule {
}
