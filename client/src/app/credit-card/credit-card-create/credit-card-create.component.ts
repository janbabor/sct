import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CreditCardService} from '../credit-card.service';
import {Luhn10} from '../../utils/luhn10.validator';

@Component({
  selector: 'sapient-credit-card-create',
  templateUrl: './credit-card-create.component.html',
  styleUrls: ['./credit-card-create.component.scss']
})
export class CreditCardCreateComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  get f(): any {
    return this.form.controls;
  }

  constructor(private formBuilder: FormBuilder, private creditCardService: CreditCardService) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      cardNumber: ['', [Validators.required, Luhn10]],
      balanceLimit: [0, [Validators.required, Validators.min(1)]]
    });
  }

  /**
   * Try to save the credit card if validation is passed;
   */
  addCreditCard() {
    this.submitted = true;
    if (this.form.valid) {
      this.creditCardService.save(this.form.value).subscribe();
    }
  }

}
