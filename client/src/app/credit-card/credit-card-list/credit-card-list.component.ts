import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CreditCardService} from '../credit-card.service';
import {Observable} from 'rxjs';
import {CreditCard} from '../credit-card';

@Component({
  selector: 'sapient-credit-card-list',
  templateUrl: './credit-card-list.component.html',
  styleUrls: ['./credit-card-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreditCardListComponent implements OnInit {

  creditCards$: Observable<CreditCard[]>;

  constructor(private creditCardService: CreditCardService) {
  }

  ngOnInit() {
    this.creditCards$ = this.creditCardService.creditCards$;
  }

}
