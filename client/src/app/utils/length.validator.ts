import {AbstractControl} from '@angular/forms';


/**
 *
 * A modified version of https://gist.github.com/DiegoSalazar/4075533, never heard of Luhn 10.
 *
 * @param control A form control
 * @returns validation object which inplies that the value is valid if the function returns null else it vill return the an error object
 */
export function Length(control: AbstractControl) {
  const value = control.value.replace(/\D/g, '');

  let check = 0;
  let digit = 0;
  let even = false;

  for (let n = value.length - 1; n >= 0; n--) {
    digit = parseInt(value.charAt(n), 10);

    if (even && (digit *= 2) > 9) {
      digit -= 9;
    }

    check += digit;
    even = !even;
  }

  return (check % 10) === 0 ? null : {validCardNumber: true};
}
