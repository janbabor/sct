###Sapient Code Test

This code test has been completed with a Spring-boot restful server and a angular application for the GUI. 
I choose Angular since it's the framework I feel most comfortable with, and I've haven't had that much time to complete this test. At the moment I have still have the unit and e2e testing left but I'll get some time on the flight to London tonight to complete that requirement and also give the code some documentation.

In Spring-boot I've used
 * JPA
 * HATEOS
 * H2
 * Hal Browser - Should only be available in a dev environment
 * Devtools - Should only be available in a dev environment
 * Lombok - For simplicity and more readable code
 
 Used https://start.spring.io/ to initialize the package. 
 I choose these packages to make the test as quick and easy for me as possible since I don't work in Java that often and these packages does it a lot better than I could ever do it. 
 I know that I'm at the moment exposing the repositories directly now which I would never have done in a real service, then I would have used at least one extra service layer before exposing any data. 
 In reality I would also validate the data that comes from the application but for this test I choose to only do it in the GUI.
 I would not expose the service to all CORS requests but in this case, I just opened all of it.
 I would probably add swagger to be able to generate an api reference 
 
 In Angular I've used 
  * Bootstrap
  * ngx-translate
  
 Used Angular-cli to generate the boilerplate application
 The only part here that is really worth testing for this scenario is the validator function. Will try to gets some time on the flight over for this. 
 If I had more time I would add both a logger module (was building one but didn't see the need in the end) and some state management for the application (ngRx)
 I would also add an http interceptor to inspect the requests
 
 How to run
####Server 
Navigate to the server directory and run `mvn install` then run `mvn spring-boot:run` to start the application. It will run a in-memory H2 database so no need for any extra work.


####Client 
Navigate to the client directory and run `npm install` or `yarn` then run `npm start` to start the application. Open chrome and navigate to `http://localhost:4200`

